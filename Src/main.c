/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2015 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "math.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim1;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM1_Init(void);
static uint16_t DeadtimeCalc(TIM_TypeDef* TIMx, uint32_t time_ns);

float sinLookUp[7200];

void CreateSinus(float *sinSrc, uint16_t len);
uint8_t freqDoubler = 1;
uint32_t pclk2Freq;
int main(void)
{

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM1_Init();
	
	CreateSinus(&sinLookUp[0], 7200);
	//PWM'ler ayni anda cikis vermeye baslamasi icin once cikislari kapatalim
	__HAL_TIM_MOE_DISABLE(&htim1);	
	HAL_TIM_PWM_Start_IT(&htim1, TIM_CHANNEL_1);
	HAL_TIMEx_PWMN_Start_IT(&htim1, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start_IT(&htim1, TIM_CHANNEL_2);
	HAL_TIMEx_PWMN_Start_IT(&htim1, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start_IT(&htim1, TIM_CHANNEL_3);
	HAL_TIMEx_PWMN_Start_IT(&htim1, TIM_CHANNEL_3);
	__HAL_TIM_ENABLE_IT(&htim1, TIM_DIER_UIE);
	__HAL_TIM_MOE_ENABLE(&htim1);
	
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

  __PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_SYSCLK|RCC_CLOCKTYPE_PCLK1
                              |RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5);

  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* TIM1 init function */
void MX_TIM1_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig;
  TIM_OC_InitTypeDef sConfigOC;

  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 1;
  htim1.Init.CounterMode = TIM_COUNTERMODE_CENTERALIGNED1;
  htim1.Init.Period = 8399;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 1;
  HAL_TIM_Base_Init(&htim1);

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig);

  HAL_TIM_PWM_Init(&htim1);

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig);

  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = DeadtimeCalc(TIM1, 300);
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig);

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1);

  HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2);

  HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3);

}

/** Pinout Configuration
*/
void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  __GPIOE_CLK_ENABLE();

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */
void CreateSinus(float *sinSrc, uint16_t len){
	uint16_t i;
	float _1degRadEqu;
	_1degRadEqu = 3.14f / 180.0f;		//1 derecenin radyan karsiligi
	
	for(i = 0; i < len; i++){
		//0'dan 2'ye kadar +1 offsetli sinus degerlerini tabloya yazalim
			*sinSrc = sinf(i * _1degRadEqu) + 1.0f;
		sinSrc++;
	}
}

uint16_t DeadtimeCalc(TIM_TypeDef* TIMx, uint32_t time_ns){
	/*
	Fdts = TCK_INT, if CKD[1:0] = 00
	Fdts = 2 x TCK_INT, if CKD[1:0] = 01
	Fdts = 4 x TCK_INT, if CKD[1:0] = 10
	*/
 
	/*
	Fdtg = Fdts, if DTG[7] = 0
	Fdtg = 2 x Fdts, if DTG[6] = 0
	Fdtg = 8 x Fdts, if DTG[5] = 0
	Fdtg = 16 x Fdts, if DTG[7:5] = 111
	*/
 
	uint16_t dtReg,ckd;
	float Fdtg,Fdts;
	Fdts = HAL_RCC_GetPCLK2Freq() * 2;

	ckd=TIMx->CR1 & 0x00000300;
	ckd=ckd>>8;
	switch(ckd)
	{
		case 0:
			ckd=1;
		break;
		case 1:
			ckd=2;
		break;
		case 2:
			ckd=4;
		break;
		default:
			ckd=10000;
		break;
	}
 
	Fdts=Fdts/ckd;		//CKD'ye b�lme islemi
	Fdts=Fdts/1000;													//ms mertebesi icin
	Fdts=Fdts/1000;													//usn icin
	Fdts=Fdts/1000;													//nsn'ye gecis
 
	/*
	1- DTG[7:5]=0xx => DT=DTG[7:0]x Fdtg with Fdtg=Fdts.
	2- DTG[7:5]=10x => DT=(64+DTG[5:0])xFdtg with Fdtg=2xFdts.
	3- DTG[7:5]=110 => DT=(32+DTG[4:0])xFdtg with Fdtg=8xFdts.
	4- DTG[7:5]=111 => DT=(32+DTG[4:0])xFdtg with Fdtg=16xFdts.
	*/
 
	TIMx->BDTR&=0xFFFFFF00;		//1. durum i�in hesaplama yapilcak
	Fdtg=Fdts;
	dtReg=time_ns*Fdtg;						
	if (dtReg <= 0x007F)
	{
		//TIMx->BDTR|=(0x0000007F & dtReg);
		return dtReg;
	}
 
	//2. Durum
	TIMx->BDTR&=0xFFFFFF00;
	TIMx->BDTR|=0x00000080;
	Fdtg=Fdts/2;
	dtReg=time_ns*Fdtg;
	dtReg-=64;
	if(dtReg <= 0x003F)
	{
		//TIMx->BDTR|=(0x0000003F & dtReg);
		return dtReg;
	}
 
	//3. Durum
	TIMx->BDTR&=0xFFFFFF00;
	TIMx->BDTR|=0x000000C0;
	Fdtg=Fdts/8;
	dtReg=time_ns*Fdtg;
	dtReg-=32;
	if(dtReg <= 0x001F)
	{
		//TIMx->BDTR|=(0x0000001F & dtReg);
		return dtReg;
	}
 
	//4. Durum
	TIMx->BDTR&=0xFFFFFF00;
	TIMx->BDTR|=0x000000E0;
	Fdtg=Fdts/16;
	dtReg=time_ns*Fdtg;
	dtReg-=32;
	if(dtReg <= 0x001F)
	{
		//TIMx->BDTR|=(0x0000001F & dtReg);
		return dtReg;
	}	
 
	TIMx->BDTR&=0xFFFFFF00;		//DeadTime degeri atanamadi.

}
#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
